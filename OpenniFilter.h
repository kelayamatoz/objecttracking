#include <pcl/io/openni_grabber.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/point_types.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <mutex>

// plane recognition
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

using namespace std;
using namespace cv;
using namespace pcl;

typedef pcl::PointXYZRGBA PointT;

class OpenniFilter
{
public:
    OpenniFilter();
    virtual ~OpenniFilter();

    /** runs the cloud viewer */
    void run();

private: 
    /** processes cloud data */
    void cloud_cb_ (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr &cloud);

    /** processes mouse actions */
    void doMouseCallback(int event, int x, int y, int flags, void* userData);
    
    /** provides an interface for opencv to utilize the call back function */
    static void mouseCallback(int event, int x, int y, int flags, void *param); 

    /** a simple cloud viewer with limited functionality */   
    pcl::visualization::CloudViewer viewer;

    /** provides interface for the viewer to display cloud data */
    pcl::Grabber* interface;

    /** intermediate frames to store processed image data */
    Mat rgbFrame, hsvFrame, grayFrame, drawing, contourMask, depthFrame;

    /** plannar segmentation */
    pcl::PassThrough<PointT> pass;
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg; 
    pcl::ExtractIndices<PointT> extract;
    pcl::ExtractIndices<pcl::Normal> extract_normals;

    /** synchronization data for the cloud thread to capture the mouse position */
    mutex mtx;
    int mouse_x = 0;
    int mouse_y = 0;
    Vec3b mousePixel;
    int avgHue;
    int avgSat;
    int avgVal;

    /** data used for filtering */
    int iLowS = 30;          
    int iHighS = 246;
    int iLowV = 54;        
    int iHighV = 255;
    int iSatDev = 27;
    int iValDev = 27;
    int iHueDev = 27;

    /** names for the windows to be displayed */
    string windowParam = "parameters";
    string windowTracker = "tracker";
    string windowFilter = "filtered frame";
    string windowDepth = "window depth";
};
