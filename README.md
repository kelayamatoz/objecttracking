**Color based object filtering using PCL, OpenCV and OpenNI**
======================
Authors: Tian Zhao
Contact: tianzhao at stanford.edu

# About: 

This project first filters out the object that contains color selected by the user. Then this project tries to find a given kind of shape from the filtered result. 

# To build: 
mkdir release
cd ./release
cmake ..
// This will build the project in the release0 folder.
make 
