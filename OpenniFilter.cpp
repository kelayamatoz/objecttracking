#include "OpenniFilter.h"

OpenniFilter::OpenniFilter() : viewer("PCL OpenNI Viewer") 
{
    // initialize display
    namedWindow(windowTracker, CV_WINDOW_AUTOSIZE);
    namedWindow(windowFilter, CV_WINDOW_AUTOSIZE);
    // namedWindow(windowDepth, CV_WINDOW_AUTOSIZE);
    setMouseCallback(windowTracker, mouseCallback, this);

    // initialize filter windows
    namedWindow(windowParam, CV_WINDOW_AUTOSIZE);
    // the hue should be in accordence with what user has chosen
    cvCreateTrackbar("Hue Deviation", windowParam.c_str(), &iHueDev, 180);
    cvCreateTrackbar("LowS",          windowParam.c_str(), &iLowS,  255); 
    cvCreateTrackbar("HighS",         windowParam.c_str(), &iHighS, 255);
    cvCreateTrackbar("LowV",          windowParam.c_str(), &iLowV,  255); 
    cvCreateTrackbar("HighV",         windowParam.c_str(), &iHighV, 255);

}

OpenniFilter::~OpenniFilter()
{
    destroyWindow(windowTracker);
    destroyWindow(windowFilter);
    destroyWindow(windowParam);
    destroyWindow(windowDepth);
}


void OpenniFilter::cloud_cb_ (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr &cloud)
{
    if (!viewer.wasStopped())
    {
        if (cloud->isOrganized())
        {
            // initialize all the Mats to store intermediate steps
            int cloudHeight = cloud->height;
            int cloudWidth = cloud->width;
            rgbFrame = Mat(cloudHeight, cloudWidth, CV_8UC3);
            depthFrame = Mat(cloudHeight, cloudWidth, CV_8UC1);
            drawing = Mat(cloudHeight, cloudWidth, CV_8UC3, NULL);
            grayFrame = Mat(cloudHeight, cloudWidth, CV_8UC1, NULL);
            hsvFrame = Mat(cloudHeight, cloudWidth, CV_8UC3, NULL);
            contourMask = Mat(cloudHeight, cloudWidth, CV_8UC1, NULL);

            if (!cloud->empty())
            {
                for (int h = 0; h < rgbFrame.rows; h ++)
                {
                    for (int w = 0; w < rgbFrame.cols; w++)
                    {
                        pcl::PointXYZRGBA point = cloud->at(cloudWidth-w-1, cloudHeight-h-1);
                        Eigen::Vector3i rgb = point.getRGBVector3i();
                        rgbFrame.at<Vec3b>(h,w)[0] = rgb[2];
                        rgbFrame.at<Vec3b>(h,w)[1] = rgb[1];
                        rgbFrame.at<Vec3b>(h,w)[2] = rgb[0];
                    }
                }

                // do the filtering 
                // color filtering based on what is chosen by users

                cvtColor(rgbFrame, hsvFrame, CV_RGB2HSV);
                int targetHue, targetSat, targetVal = 0;

                Vec3b pixel;
                int xPos, yPos = 0;
                mtx.lock();
                
                if (mouse_x == 0 && mouse_y == 0)
                    pixel = rgbFrame.at<Vec3b>(0,0); //default
                else
                {
                    xPos = mouse_x;
                    yPos = mouse_y;
                    pixel = mousePixel;
				}

                mtx.unlock();

                int hueLow = pixel[0] < iHueDev ? 0: pixel[0] - iHueDev;
                int hueHigh = pixel[0] > 180 - iHueDev ? 180 : pixel[0] + iHueDev;
                // Now we consider the high saturation part...... this part needs to be tested under various lab settings and see if it is always working fine...
                int newSatHigh = (int) (pixel[1] * 2);
                iHighS = newSatHigh > 255 ? 255 : newSatHigh ;
                iLowS = iHighS / 4;

                inRange(hsvFrame, Scalar(hueLow, iLowS, iLowV), Scalar(hueHigh, iHighS, iHighV), grayFrame);

                // // removes small objects from the foreground by morphological opening
                erode(grayFrame, grayFrame, getStructuringElement(MORPH_ELLIPSE, Size(5,5)));
                dilate(grayFrame, grayFrame, getStructuringElement(MORPH_ELLIPSE, Size(5,5)));

                // morphological closing (removes small holes from the foreground)
                dilate(grayFrame, grayFrame, getStructuringElement(MORPH_ELLIPSE, Size(5,5)));
                erode(grayFrame, grayFrame, getStructuringElement(MORPH_ELLIPSE, Size(5,5)));

                pcl::PointCloud<pcl::PointXYZRGBA>::Ptr resultCloud (new pcl::PointCloud<pcl::PointXYZRGBA>);
                pcl::PointXYZRGBA newPoint;
                for (int h = 0; h < grayFrame.rows; h ++)
                {
                    for (int w = 0; w < grayFrame.cols; w++)
                    {
                        if (grayFrame.at<uchar>(h,w) > 0)
                        {
                            newPoint = cloud->at(cloudWidth-w-1, cloudHeight-h-1);
                            resultCloud->push_back(newPoint);
                        }
                    }
                }

                // filtering using plannar matching. Currently using a 2d circle.
                pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());
                PointCloud<PointT>::Ptr cloud_inline (new pcl::PointCloud<PointT>);
                PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
                PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
        
                ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients);
                PointIndices::Ptr inliers_plane (new pcl::PointIndices);

                pass.setInputCloud (resultCloud);
                pass.setFilterFieldName ("z");
                pass.setFilterLimits (0, 1.5);
                pass.filter (*cloud_filtered);

                // Estimate point normals
                ne.setSearchMethod (tree);
                ne.setInputCloud (cloud_filtered);
                ne.setKSearch (50);
                ne.compute (*cloud_normals);

                // Create the segmentation object for the planar model and set all the parameters
                // Try drawing the orientation on the object filtered. (XYZ)
                seg.setOptimizeCoefficients (true);
                seg.setModelType (pcl::SACMODEL_CIRCLE2D);
                seg.setNormalDistanceWeight (0.1);
                seg.setMethodType (pcl::SAC_RANSAC);
                seg.setMaxIterations (10);
                seg.setDistanceThreshold (0.03);
                seg.setInputCloud (cloud_filtered);
                seg.setInputNormals (cloud_normals);
                
                // Obtain the plane inliers and coefficients
                seg.segment (*inliers_plane, *coefficients_plane);

                extract.setInputCloud (cloud_filtered);
                extract.setIndices (inliers_plane);
                extract.setNegative (false);            

                pcl::PointCloud<PointT>::Ptr cloud_plane (new pcl::PointCloud<PointT> ());
                extract.filter (*cloud_plane);

                if (xPos == 0 && yPos == 0)
                    viewer.showCloud(cloud);
                else
                    viewer.showCloud(cloud_plane);
                
                imshow(windowTracker, rgbFrame);
                imshow(windowFilter, grayFrame);
                char key = waitKey(1);
                if (key == 27) 
                {
                    interface->stop();
                    return;
                }
            }
            else
                cout << "Warning: Point Cloud is empty" << endl;
        }
        else
            cout << "Warning: Point Cloud is not organized" << endl;
    }
}

void OpenniFilter::run ()
{
    interface = new pcl::OpenNIGrabber();
    boost::function<void (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> f =
        boost::bind (&OpenniFilter::cloud_cb_, this, _1);

    interface->registerCallback(f);
    interface->start();
    while (!viewer.wasStopped())
    {
        boost::this_thread::sleep (boost::posix_time::seconds (1));
    }

    interface->stop();
}

void OpenniFilter::mouseCallback(int event, int x, int y, int flags, void *param)
{
    OpenniFilter *self = static_cast<OpenniFilter *>(param);
    self->doMouseCallback(event, x, y, flags, NULL);
}

void OpenniFilter::doMouseCallback(int event, int x, int y, int flags, void* userData)
{
    if (event == EVENT_RBUTTONDOWN)
    {
        mtx.lock();
        mouse_x = x;
        mouse_y = y;
        mousePixel = hsvFrame.at<Vec3b>(mouse_x,mouse_y);

        // choosing the nearby 24 points and use average value
        // TODO: check bound later
        int boundSize = 5;
        int xStart = mouse_x - boundSize / 2;
        int yStart = mouse_y - boundSize / 2;
        double sumHue, sumSat, sumVal;
        double avgHue, avgSat, avgVal;

        for (int i = 0; i < 5; i ++)
        {
        	for (int j = 0; j < 5; j ++)
        	{
        		Vec3b tempPixel = hsvFrame.at<Vec3b>(xStart + i, yStart + j);
        		sumHue += tempPixel[0];
        		sumSat += tempPixel[1];
        		sumVal += tempPixel[2];
        	}
        }

        avgHue = sumHue/25;
        avgSat = sumSat/25;
        avgVal = sumVal/25;
        cout << "HSV of selected pixel: Hue = " << (double)mousePixel[0] << ", Saturation = " 
            << (double)mousePixel[1] << ", Value = " << (double)mousePixel[2] << ". " << endl;
        cout << "avgHSV of selected pixel: avgHue = " << (double)avgHue << ", avgSat = "
            << (double)avgSat << ", avgVal = " << (double)avgVal << ". " << endl;
        mousePixel[0] = (int)avgHue;
        mousePixel[1] = (int)avgSat;
        mousePixel[2] = (int)avgVal;
        mtx.unlock();
    }
}
